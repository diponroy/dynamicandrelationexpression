﻿using System.Collections.Generic;
using DynamicAndRelationExpression.DAO;

namespace DynamicAndRelationExpression.Data
{
    internal class PersonUtility
    {
        public List<Person> GetAll()
        {
            return new List<Person>
            {
                new Person {Name = "Dipon", Age = 23, Sex = "Male", IsValid = true},
                new Person {Name = "Dipa", Age = 17, Sex = "Female", IsValid = true},
                new Person {Name = "Dip", Age = 16, Sex = "Male", IsValid = true},
                new Person {Name = "Dipika", Age = 14, Sex = "Female", IsValid = true},
                new Person {Name = "Dipon", Age = 23, Sex = "Male", IsValid = false},
                new Person {Name = "Dipa", Age = 17, Sex = "Female", IsValid = false},
                new Person {Name = "Dip", Age = 16, Sex = "Male", IsValid = false},
                new Person {Name = "Dipika", Age = 14, Sex = "Female", IsValid = false},
            };
        }
    }
}
