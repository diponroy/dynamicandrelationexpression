﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using DynamicAndRelationExpression.DAO;
using DynamicAndRelationExpression.Utility;
using NUnit.Framework;

namespace DynamicAndRelationExpression.Tests
{
    [TestFixture]
    public class AndRelationExpression
    {
        [Test]
        public void AssignedThreeValues()
        {
            /*data*/
            var list = new List<Person>
            {
                new Person {Name = "Dipon", Age = 23, Sex = "Male", IsValid = true}, //select this one
                new Person {Name = "Dipa", Age = 17, Sex = "Female", IsValid = true},
                new Person {Name = "Dip", Age = 16, Sex = "Male", IsValid = true},
            };

            var aPerson = new Person {Name = "Dipon", Age = 23, Sex = "Male"};
            Expression<Func<Person, bool>> lambda = aPerson.GetExpretion();
            List<Person> persons = list.Where(lambda.Compile()).ToList();

            Assert.AreEqual(1, persons.Count);
        }

        [Test]
        public void AssignedOneValues()
        {
            /*data*/
            var list = new List<Person>
            {
                new Person {Name = "Dipon", Age = 23, Sex = "Male", IsValid = true}, //select this one
                new Person {Name = "Dipa", Age = 17, Sex = "Female", IsValid = true}, //select this one too
                new Person {Name = "Dip", Age = 16, Sex = "Male", IsValid = false},
            };

            var aPerson = new Person {IsValid = true};
            Expression<Func<Person, bool>> lambda = aPerson.GetExpretion();
            List<Person> persons = list.Where(lambda.Compile()).ToList();

            Assert.AreEqual(2, persons.Count);
        }

        [Test]
        public void AssignedTwoValues()
        {
            /*data*/
            var list = new List<Person>
            {
                new Person {Name = "Dipon", Age = 23, Sex = "Male", IsValid = true}, //select this one
                new Person {Name = "Dipa", Age = 17, Sex = "Female", IsValid = false},
                new Person {Name = "Dip", Age = 16, Sex = "Male", IsValid = true}, //select this one
            };

            var aPerson = new Person {Sex = "Male", IsValid = true};
            Expression<Func<Person, bool>> lambda = aPerson.GetExpretion();
            List<Person> persons = list.Where(lambda.Compile()).ToList();

            Assert.AreEqual(2, persons.Count);
        }

        [Test]
        public void AssignedNoValues_Throws_Exception()
        {
            /*data*/
            var list = new List<Person>
            {
                new Person {Name = "Dipon", Age = 23, Sex = "Male", IsValid = true}, //select this one
                new Person {Name = "Dipa", Age = 17, Sex = "Female", IsValid = false},
                new Person {Name = "Dip", Age = 16, Sex = "Male", IsValid = true}, //select this one
            };

            var aPerson = new Person();
            Assert.Throws<Exception>(() => aPerson.GetExpretion());
        }
    }
}
