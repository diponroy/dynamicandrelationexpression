﻿using System.Collections.Generic;
using DynamicAndRelationExpression.DAO.Shared;

namespace DynamicAndRelationExpression.DAO
{
    internal class Person : FieldValueChangedTracker
    {
        #region Members

        private long _id;
        private string _name;
        private short _age;
        private bool _isValid;
        private double _yearlyIncome;
        private string _sex;

        #endregion

        #region Constructor

        public Person()
        {
            ChangedPropertyList = new List<string>();
        }

        #endregion

        #region Propertypes

        public long Id
        {
            get { return _id; }
            set
            {
                _id = value;
                ReportValueChanged(Utility.ValueChangeUtility.GetName(() => Id));
                ;
            }
        }

        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                ReportValueChanged(Utility.ValueChangeUtility.GetName(() => Name));
            }
        }

        public short Age
        {
            get { return _age; }
            set
            {
                _age = value;
                ReportValueChanged(Utility.ValueChangeUtility.GetName(() => Age));
            }
        }

        public bool IsValid
        {
            get { return _isValid; }
            set
            {
                _isValid = value;
                ReportValueChanged(Utility.ValueChangeUtility.GetName(() => IsValid));
            }
        }

        public double YearlyIncome
        {
            get { return _yearlyIncome; }
            set
            {
                _yearlyIncome = value;
                ReportValueChanged(Utility.ValueChangeUtility.GetName(() => YearlyIncome));
            }
        }

        public string Sex
        {
            get { return _sex; }
            set
            {
                _sex = value;
                ReportValueChanged(Utility.ValueChangeUtility.GetName(() => Sex));
            }
        }

        public new List<string> ChangedPropertyList { get; private set; }

        #endregion

        protected override void ReportValueChanged(string propertyName)
        {
            Utility.ValueChangeUtility.ReportValueChanged(propertyName, this);
        }
    }
}
