﻿using System.Collections.Generic;

namespace DynamicAndRelationExpression.DAO.Shared
{
    internal abstract class FieldValueChangedTracker
    {
        protected List<string> ChangedPropertyList { get; set; }
        protected abstract void ReportValueChanged(string propertyName);
    }
}