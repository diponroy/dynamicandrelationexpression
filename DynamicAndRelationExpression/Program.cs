﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using DynamicAndRelationExpression.DAO;
using DynamicAndRelationExpression.Data;
using DynamicAndRelationExpression.Utility;

namespace DynamicAndRelationExpression
{
    class Program
    {
        static void Main(string[] args)
        {
            /*Any assigned property will be turend into and*/
            Person aPerson = new Person() {Name = "Dipon", Age = 23, Sex = "Male"};
            /*the dynamic expression*/
            Expression<Func<Person, bool>> lambda = aPerson.GetExpretion();
            /*resul list*/
            List<Person> persons = new PersonUtility().GetAll().Where(lambda.Compile()).ToList();

            Console.ReadKey();
        }
    }
}
