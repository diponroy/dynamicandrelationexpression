﻿using System;
using System.Linq.Expressions;
using System.Reflection;

namespace DynamicAndRelationExpression.Utility
{
    public static class LambdaUtility
    {
        public static Expression<Func<TType, bool>> GetExpretion<TType>(this TType sourceObj)
        {
            try
            {
                // find the property named ChangedPropertyList, which is required must
                PropertyInfo prty = sourceObj.GetType().GetProperty("ChangedPropertyList");
                if (prty == null)
                {
                    throw new NotImplementedException("the proertyContainer must have List<string>ChangedPropertyList");
                }
                dynamic changedPropertyNames = prty.GetValue(sourceObj, null);

                //build the Expression parm
                ParameterExpression parameterExpression = Expression.Parameter(sourceObj.GetType(), "x");
                Expression totalExpression = null;

                //foreach property name is value changed list
                foreach (string aName in changedPropertyNames)
                {
                    //get a property detail
                    PropertyInfo property = sourceObj.GetType().GetProperty(aName);
                    string propertyName = property.Name;
                    var propertyValue = property.GetValue(sourceObj, null);
                    Type propertyType = property.PropertyType;

                    //Make the Expression
                    Expression namePoperty = Expression.Property(parameterExpression, property);
                    ConstantExpression valueProperty = Expression.Constant(propertyValue);
                    Expression crntExpretion = Expression.Equal(namePoperty, valueProperty);
                    totalExpression = (totalExpression != null)
                                        ? Expression.AndAlso(totalExpression, crntExpretion)
                                        : crntExpretion;
                }

                if (totalExpression != null)
                {
                    var resultLamdaExpretion = Expression.Lambda<Func<TType, bool>>(totalExpression, parameterExpression);
                    return resultLamdaExpretion;
                }
                else
                {
                    throw new NullReferenceException("for lamda expretion get opretion the obj should be null");
                }
            }
            catch (Exception exception)
            {
                throw new Exception("Error in expretion building", exception);
            }
        }
    }
}
