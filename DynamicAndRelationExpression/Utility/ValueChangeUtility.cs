﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace DynamicAndRelationExpression.Utility
{
    internal static class ValueChangeUtility
    {
        public static string GetName<TSource>(Expression<Func<TSource>> sourceObj)
        {
            MemberExpression expression = (MemberExpression)sourceObj.Body;
            return expression.Member.Name;
        }

        public static void ReportValueChanged<TSource>(string propertyName, TSource propertyContainer)
        {
            PropertyInfo prty = propertyContainer.GetType().GetProperty("ChangedPropertyList");
            if (prty == null)
            {
                throw new NotImplementedException("the proertyContainer must have List<string>ChangedPropertyList");
            }
            dynamic value = prty.GetValue(propertyContainer, null);
            value.Add(propertyName);
        }

        public static bool IsChanged<TSource>(string propertyName, TSource propertyContainer)
        {
            PropertyInfo prty = propertyContainer.GetType().GetProperty("ChangedPropertyList");
            if (prty == null)
            {
                throw new NotImplementedException("the proertyContainer must have List<string>ChangedPropertyList");
            }
            dynamic value = prty.GetValue(propertyContainer, null);
            bool doesExist = (value.Contains(propertyName)) ? true : false;
            return doesExist;
        }
    }
}